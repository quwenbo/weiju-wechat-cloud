// pages/my/myComment/myComment.js
import {getUserComment} from "../../../api/index"
import {timeFormat} from "../../../utils/util"
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    CustomBar: app.globalData.CustomBar,
    tabActive:"appeal",
    commentList:[],
    page:1,
    loading:false,
    finishStatus:false,
    refreshStatus:false  //下拉刷新状态
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.getUserComment()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  tabClick:function(e){
    const type = e.currentTarget.dataset.type
    this.setData({
      tabActive:type,
      commentList:[],
      finishStatus:false
    })
    console.log(type)
    this.getUserComment({type:type,page:1,number:20})
  },
  commentClick:function(e){
    const type = this.data.tabActive
    const id = e.currentTarget.dataset.dynamicid 
    if(type==="dynamic"){
      wx.navigateTo({
        url: '/pages/dynamicDetail/dynamicDetail?dynamicId='+id,
      })
    }
  },
  scrollBottomEvent:function(){
    if(!this.data.finishStatus){
      this.setData({
        page:this.data.page+1,
      })
      this.getUserComment()
    }
  },
  refreshEvent:function(){
    //下拉刷新
    console.log("asdasd")
    this.setData({
      page:1,
      commentList:[],
      finishStatus:false
    })
    this.getUserComment()
  },
  getUserComment: async function(){
    if(this.data.finishStatus){
      return 
    }
    this.setData({
      loading:true
    })
    const result = await getUserComment({type:this.data.tabActive,page:this.data.page,number:20})
    result.data.map(item=>{
      item.createTime = timeFormat(item.createTime)
    })
    this.setData({
      commentList:this.data.commentList.concat(result.data),
      loading:false,
      finishStatus:result.data.length===20?false:true,
      refreshStatus:false
    })
    console.log(result)
  }
})
//获取用户的评论
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库
const userComment = async function (params) {
  const page = params.page ? params.page - 1 : 0 //当前页
  const number = params.number ? params.number : 10 //单页的数据数量
  const countResult = await db.collection('dynamic').count()
  const total = countResult.total
  const wxContext = cloud.getWXContext()
  //用户是否的判断
  // 计算需分几次取
  const batchTimes = Math.ceil(total / 100)
  // let listResult = await db.collection('dynamic').skip(page * number).limit(number).get()
  // 聚合查询用户的信息
  let resultList = {}
  if (params.type === "dynamic") {
    resultList = await db.collection('dynamicComment').aggregate().match({
        _openid: wxContext.OPENID
      })
      .sort({
        createTime: -1
      }).skip(page * number).limit(number)
      .end()

  } else if(params.type==="appeal") {
    resultList = await db.collection('appealComment').aggregate().match({
        _openid: wxContext.OPENID
      })
      .sort({
        createTime: -1
      }).skip(page * number).limit(number)
      .end()
  }


  return {
    data: resultList.list,
    page: page + 1,
    number: number,
    totalPage: batchTimes
  }
}
module.exports = userComment
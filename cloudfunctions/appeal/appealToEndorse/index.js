// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 点赞诉求接口
const appealToEndorse = async function (params) {
  const wxContext = cloud.getWXContext()

  let data = {
    _openid: wxContext.OPENID,
    appealId: params.appealId,
    createTime: db.serverDate(),
    updateTime: db.serverDate(),
  }

  try {

    let result = await db.collection('appealEndorse').add({data: data})
    
    return result
    
  } catch (error) {

    console.log('appealToEndorse error' + error)

    return false
    
  }

  

}

module.exports = appealToEndorse